import React from 'react';
import './ProductList.css'

export class ProductList extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            source: []
        };
    }
    
    render(){
        let names = this.props.source.map((item, index) => 
            <div key={index}>{item.productName}</div>
            
        );
        let prices = this.props.source.map((item, index) => 
            <div key={index}>{item.price}</div>
        );
        
       
        return (
            <React.Fragment>
            <div>
                <h1>{this.props.title}</h1>
                <div className="items-container">
                {names}
                </div>
                
            </div>
            </React.Fragment>
        )    
    }
}